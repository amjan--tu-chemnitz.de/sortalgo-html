from .sort_algorithm import sort_algorithm

class bubblesort(sort_algorithm):

	def __init__(self, array: list):
		super().__init__(array)
		self.bubblesort()
		self.next_step()

	def swap(self, i: int, j: int):
		tmp = self.array[i]
		self.array[i] = self.array[j]
		self.array[j] = tmp

	def bubblesort(self):
		swapped = False
		for j in range(len(self.array) - 1):
			for i in range(len(self.array) - j - 1):
				self.next_step()
				self.mark_index(i)
				self.mark_index(i + 1)
				if self.array[i] > self.array[i + 1]:
					self.swap(i, i + 1)
					swapped = True
					self.next_step();
					self.mark_index(i)
					self.mark_index(i + 1)
			if not swapped:
				break
