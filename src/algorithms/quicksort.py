from .sort_algorithm import *
from random import randrange

class quicksort(sort_algorithm):
    
    def __init__(self, array: list):
        super().__init__(array)
        self.quicksort(0, len(array) - 1)

    def print_array(self):
        for e in self.array:
            print(e.value)
    
    def quicksort(self, left, right):
        self.next_step()
        if left < right:
            self.next_step()
            splitter = self.split(left, right)
            self.quicksort(left, splitter - 1)
            self.quicksort(splitter + 1, right)

    def split(self, left, right):
        self.next_step()
        self.mark_permanent(left)
        self.mark_permanent(right)
        i = left
        j = right - 1
        pivot = self.array[right]
        while i < j:
            self.next_step()
            while i < j and self.array[i] <= pivot:
                i += 1
            while j > i and self.array[j] > pivot:
                j -= 1

            self.mark_index(i, Color.BLUE)
            self.mark_index(j, Color.GREEN)

            if self.array[i] > self.array[j]:
                self.next_step()
                self.mark_index(i, Color.CYAN)
                self.mark_index(j, Color.CYAN)
                self.array[i], self.array[j] = self.array[j], self.array[i]
                self.next_step()

        self.next_step()
        self.mark_index(i, Color.BLUE)
        self.mark_index(right, Color.PURPLE)
        if self.array[i] > pivot:
            self.next_step()
            self.mark_index(i, Color.CYAN)
            self.mark_index(right, Color.CYAN)
            self.array[i], self.array[right] = self.array[right], self.array[i]
            self.next_step()
        else:
            i = right
        self.next_step()
        self.unmark_permanent(left)
        self.unmark_permanent(right)
        return i
            
