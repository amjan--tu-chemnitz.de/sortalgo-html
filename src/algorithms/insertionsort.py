from .sort_algorithm import *

class insertionsort(sort_algorithm):
	
	def __init__(self, array: list):
		super().__init__(array)
		self.insertionsort()
		self.next_step()

	def insertionsort(self):
		n = len(self.array)
	
		for i in range(1, n):
			self.next_step()
			# find element that is in wrong position
			wpos = -1
			for j in range(i, n):
				self.next_step()
				self.mark_index(j)
				self.mark_index(j - 1)
				if self.array[j - 1] > self.array[j]:
					wpos = j
					break

			if wpos == -1:
				# all in correct order
				break

			# find correct position
			key = self.array[wpos]
			cpos = wpos
			while cpos > 0 and self.array[cpos - 1] > key:
				cpos -= 1
				self.next_step()
				self.mark_index(wpos, Color.BLUE)
				self.mark_index(cpos, Color.BLUE)

			# move element to correct position
			self.array.pop(wpos)
			self.array.insert(cpos, key)
				
