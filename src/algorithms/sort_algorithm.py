import json
from enum import Enum

class Color(Enum):
    RED = 0
    GREEN = 1
    BLUE = 2
    PURPLE = 3
    YELLOW = 4
    CYAN = 5

class sort_item:
    _id_counter = -1

    @classmethod
    def generate_id(cls):
        cls._id_counter += 1
        return cls._id_counter

    def __init__(self, value: int):
        self.value = value
        self.id = sort_item.generate_id()

    def __lt__(self, other):
        return self.value < other.value

    def __eq__(self, other):
        return self.value == other.value

    def __gt__(self, other):
        return self.value > other.value

    def __le__(self, other):
        return self.value <= other.value

    def __ge__(self, other):
        return self.value >= other.value

class sort_step:
    def __init__(self, step: list, marked: list = []):
        self.array = step.copy()
        self.marked = marked.copy()

def convert_to_dict(obj):
    if isinstance(obj, sort_step):
        return {"array": obj.array, "marked": obj.marked}
    elif isinstance(obj, sort_item):
        return {"value": obj.value, "id": obj.id}

class sort_algorithm:

    def __init__(self, startarray: list):
        self.array = [sort_item(num) for num in startarray]
        self.steps = []
        self.permamarked = []
        self.next_step()

    def next_step(self):
        step = sort_step(self.array)
        step.marked = self.permamarked + step.marked
        self.steps.append(step)

    def mark_index(self, index: int, color: Color = Color.RED):
        self.steps[-1].marked += [index, color.value]

    def mark_permanent(self, index: int, color: Color = Color.RED):
        self.permamarked += [index, color.value]

    def unmark_permanent(self, index: int):
        for i in range(0, len(self.permamarked), 2):
            if i < len(self.permamarked) and self.permamarked[i] == index:
                self.permamarked.pop(i)
                self.permamarked.pop(i)

    def convert_to_json(self):
        return json.dumps(self.steps,
                          default=convert_to_dict).replace('\n', '')

