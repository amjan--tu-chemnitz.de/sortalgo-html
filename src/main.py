from algorithms.bubblesort import bubblesort
from algorithms.insertionsort import insertionsort
from algorithms.quicksort import quicksort

from htmlbuilder.builder import builder

from random import randrange
import os

array = []

for i in range(15):
	val = randrange(1, 40)
	array.append(val)

ins = insertionsort(array)
bbs = bubblesort(array)
qst = quicksort(array)

if not os.path.exists("../test"):
	os.makedirs("../test")

builder.assemble_to_file(ins, "../test/insertionsort.html")
builder.assemble_to_file(bbs, "../test/bubblesort.html")
builder.assemble_to_file(qst, "../test/quicksort.html")
