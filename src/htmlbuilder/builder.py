import os

class builder:

    @staticmethod
    def read_template():
        classdir = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(classdir, "template.html")
        content = ""
        with open(path, 'r') as file:
            content = file.read()
        return content
    
    @staticmethod
    def assemble_html(algo):
        pattern = "/*INSERT*/"
        return builder.read_template().replace(pattern, algo.convert_to_json())

    @staticmethod
    def assemble_to_file(algo, path):
        html = builder.assemble_html(algo)
        with open(path, 'w') as file:
            file.write(html)

